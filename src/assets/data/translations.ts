import { Translation } from 'src/app/models/models';

export const translations: Translation[] = [
  {
    code: 'gr',
    walkIn: 'Είσοδος χωρίς κράτηση',
    waitingList: 'Εγγραφείτε στη Λίστα Αναμονής',
    back: 'Πίσω',
    name: 'Όνομα',
    surname: 'Επώνυμο',
    email: 'Email',
    countryCode: 'Κωδικός Χώρας',
    phone: 'Τηλέφωνο',
    guests: 'Επισκέπτες',
    checkTerms:
      'Συναινώ στη χρήση των στοιχείων μου από το κατάστημα για διαφημιστικούς σκοπούς.',
    submit: 'Υποβολή',
    alerts: {
      emptyFields: ' Υποχρεωτικά πεδία (*)',
      invalidEmail: 'Το email να είναι έγκυρο',
      invalidPhone: 'Το τηλέφωνο να είναι έγκυρο',
    },
  },
  {
    code: 'en',
    walkIn: 'Entrance without Reservation',
    waitingList: 'Join Waiting List',
    back: 'Back',
    name: 'Name',
    surname: 'Surname',
    email: 'Email',
    countryCode: 'Country Code',
    phone: 'Phone',
    guests: 'Guests',
    checkTerms: 'I agree to use my information for advertisement reasons. ',
    submit: 'Submit',
    alerts: {
      emptyFields: 'Required fields (*)',
      invalidEmail: 'Email must be valid',
      invalidPhone: 'Phone must be valid',
    },
  },
  {
    code: 'ru',
    walkIn: 'Вход без бронирования',
    waitingList: 'Очередь',
    back: 'Назад',
    name: 'Имя',
    surname: 'Фамилия',
    email: 'Эл. адрес',
    countryCode: 'Код страны',
    phone: 'Телефон',
    guests: 'гости',
    checkTerms:
      'Я согласен получать информацию о будущих мероприятиях и акциях',
    submit: 'Отправить',
    alerts: {
      emptyFields: 'Обязательные поля (*)',
      invalidEmail: 'Эл. адрес должен быть действительным',
      invalidPhone: 'Телефон должен быть действительным',
    },
  },
];
