export interface LangInfo {
  name: string;
  code: string;
  iconURL: string;
  messages: {
    welcome: string;
    thankYou: string;
  };
  link: {
    href: string;
    text: string;
  };
}

// ==========================================================

export interface CurrentStore {
  id: number;
  name: string;
  cssUrl: string;

  enableWalkIn: boolean;
  enableWaitingList: boolean;

  languages: LangInfo[];
}

// ==========================================================

export interface Customer {
  id: number;
  surname: string;
  name: string;
  email: string;
  countryTelCode: CountryTelCode | null;
  phone: string;
  guests: string;
  optionStatus: string;
  checkTerms: boolean;
  arrivalTime: string;
}

// ==========================================================

export interface CountryTelCode {
  country: string;
  code: string;
}

export interface Translation {
  code: string;
  walkIn: string;
  waitingList: string;
  back: string;
  name: string;
  surname: string;
  email: string;
  countryCode: string;
  phone: string;
  guests: string;
  checkTerms: string;
  submit: string;
  alerts: {
    emptyFields: string;
    invalidEmail: string;
    invalidPhone: string;
  };
}
