// angular
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
//rxjs
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
//models
import { CurrentStore, Customer } from '../models/models.js';
//environment
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  // ================= Properties =================
  // json-server
  private restaurantsUrl = 'api/restaurants';
  // environment
  private apiUrl = environment.apiUrl;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };
  headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  // constructor
  constructor(private http: HttpClient) {}

  // ================= CUSTOMERS =================

  /** GET CurrentStore by id. Will 404 if id not found */
  getAllCustomers() {
    return this.http.get(`${this.apiUrl}/customers`).subscribe((res) => {
      console.log(res);
    });
  }

  // WalkIn Post
  addCustWalkIn(customer: Customer): Observable<Customer> {
    return this.http
      .post<Customer>(`${this.apiUrl}/customers`, customer, this.httpOptions)
      .pipe(catchError(this.handleError<Customer>('addCustomer')));
  }

  // WaitList Post
  addCustWaitList(customer: Customer): Observable<Customer> {
    return this.http
      .post<Customer>(`${this.apiUrl}/customers`, customer)
      .pipe(catchError(this.handleError<Customer>('addCustomer')));
  }

  // ================= RESTAURANTS =================

  //getRestaurants
  getRestaurants(): Observable<CurrentStore[]> {
    return this.http.get<CurrentStore[]>(this.restaurantsUrl).pipe(
      tap((_) => console.log('fetched restaurants')),
      catchError(this.handleError<CurrentStore[]>('getRestaurants', []))
    );
  } // end of getUsers

  //getAPIRestaurants
  getAPIRestaurants(): Observable<CurrentStore[]> {
    return this.http.get<CurrentStore[]>(`${this.apiUrl}/restaurants`);
  } // end of getUsers

  /** GET CurrentStore by id. Will 404 if id not found */
  getRestaurant(id: number): Observable<CurrentStore> {
    const url = `${this.restaurantsUrl}/${id}`;
    return this.http
      .get<CurrentStore>(url)
      .pipe(
        catchError(this.handleError<CurrentStore>(`getRestaurant id=${id}`))
      );
  }

  // ================= ERROR HANDLERS =================

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   *
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  } /// end of handleError
} // end of class
