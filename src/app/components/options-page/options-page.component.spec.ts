import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { OptionsPageComponent } from './options-page.component';
import { By } from '@angular/platform-browser';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { translations } from 'src/assets/data/translations';

describe('OptionsPageComponent', () => {
  let component: OptionsPageComponent;
  let fixture: ComponentFixture<OptionsPageComponent>;
  const activatedRouteMock = {
    paramMap: of({ get: (param: string) => '1' }), // Replace '123' with the desired item id for testing
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatSelectModule, MatCheckboxModule, FormsModule],
      declarations: [OptionsPageComponent],
      providers: [{ provide: ActivatedRoute, useValue: activatedRouteMock }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  //Screen#1 - Language Buttons
  it('(LANGUAGE) should toggle enableLangList boolean value', () => {
    component.enableLangList = false;
    let btn = fixture.nativeElement.querySelector('.content').click();
    expect(component.enableLangList).toBe(true);

    component.enableLangList = true;
    btn = fixture.nativeElement.querySelector('.content').click();
    expect(component.enableLangList).toBe(false);
  });

  //Screen#1 - Options Buttons
  it('(OPTIONS) should change walkedIn to true', () => {
    component.walkedIn = false;
    component.walkIn();
    expect(component.walkedIn).toBe(true);
  });

  it('(OPTIONS) should change waitedList to true', () => {
    component.waitedList = false;
    component.waitingList();
    expect(component.waitedList).toBe(true);
  });

  //Screen#2 - Form Submission

  // #Surname
  it('(FORM) should have an input with id "surname"', () => {
    component.waitedList = true;
    fixture.detectChanges();
    const inpSur = fixture.debugElement.query(By.css('#surname'));
    expect(inpSur).toBeTruthy();
  }); // #EndSurname

  // Surname and type text
  it('(FORM) should have an input with id "surname" and type "text"', () => {
    component.waitedList = true;
    fixture.detectChanges();
    const inpSur = fixture.debugElement.query(By.css('#surname'));
    expect(inpSur.nativeElement.type).toBe('text');
  }); // #EndSurnameAndType

  // #Name
  it('(FORM) should display Surname* on the <label> for the surname field', () => {
    component.waitedList = true;
    fixture.detectChanges();

    const label = fixture.debugElement.query(By.css('.ctrl-label'));

    let liLangBtnEn = fixture.nativeElement.querySelector('#en').click();
    component.selectedTranslation = translations[0];
    fixture.detectChanges();
    expect(label.nativeElement.innerText).toEqual('Surname*');
  }); // #EndName

  // #region Layout HTML related tests
  it('(HTML) should have a div element with class "app-container"', () => {
    const divAppContainer = fixture.debugElement.query(
      By.css('.app-container')
    );
    expect(divAppContainer).toBeTruthy();
  }); // #end region

  // #region Form HTML related tests
  it('(HTML) should have a form with class "form"', () => {
    component.walkedIn = true;
    component.waitedList = true;
    component.submittedWalkIn = false;
    component.submittedWaitingList = false;
    fixture.detectChanges();
    const el = fixture.debugElement.query(By.css('.form'));
    expect(el).toBeTruthy();
  }); // #end region

  // #Spy Form Submission
  it('(SPY FORM)should call the submit method when the form is submitted', () => {
    component.walkedIn = true;
    component.waitedList = true;
    component.submittedWalkIn = false;
    component.submittedWaitingList = false;
    fixture.detectChanges();

    // Arrange
    // component.isEmptyField = false;
    fixture.detectChanges();
    const el = fixture.debugElement.query(By.css('.submit-btn'));
    const fnc = spyOn(component, 'submit');

    // Act
    el.triggerEventHandler('click', null);

    // Assert
    expect(fnc).toHaveBeenCalled();
  }); // #EndSpyFormSubmission

  it('(SPY FORM)should call the submit method when the form is submitted', () => {
    component.walkedIn = true;
    component.waitedList = true;
    component.submittedWalkIn = false;
    component.submittedWaitingList = false;
    fixture.detectChanges();

    // Arrange
    const el = fixture.debugElement.query(By.css('.submit-btn'));
    expect(el).toBeTruthy();
  });

  // it('should display Επώνυμο* on the label for the surname field', () => {
  //   let liLang = fixture.debugElement.query(By.css('.li-lang'));
  //   const label = fixture.debugElement.query(By.css('.ctrl-label'));
  //   // component.translation = component.translations[1];
  //   let eng = fixture.debugElement.query(By.css('#ru'));

  //   liLang.triggerEventHandler('click', eng.nativeElement);
  //   fixture.detectChanges();
  //   component.selectedTranslation = translations[0];
  //   // component.translation = component.selectedTranslation;
  //   console.log(component.translation);
  //   // expect(label.nativeElement.textContent).toContain('Surname*');
  //   // if (translations[1].code === 'en') {
  //   // expect(label.nativeElement.textContent).toContain('Surname*');
  //   // expect(label.nativeElement.innerText).toEqual(
  //   //   `${component.selectedTranslation.surname}*`
  //   // );

  //   expect(label.nativeElement.innerText).toEqual('Фамилия*');
  //   // }
  // });

  // Create component
  it('(CREATE) should create OptionsPageComponent', () => {
    expect(component).toBeTruthy();
  });
}); // End of options-page.component.spec.ts
