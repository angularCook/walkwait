import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Renderer2, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

//models
import {
  CurrentStore,
  LangInfo,
  Translation,
  Customer,
  CountryTelCode,
} from 'src/app/models/models';
//services
import { DataService } from '../../services/data.service';
//data
import { translations } from '../../../assets/data/translations';
import { countryTelCodes } from '../../../assets/data/countryTelCodes';

@Component({
  selector: 'app-options-page',
  templateUrl: './options-page.component.html',
  styleUrls: ['./options-page.component.scss'],
})
export class OptionsPageComponent implements OnInit {
  // ========== Properties ==========
  id: string = 'id';
  restaurant: CurrentStore = {} as CurrentStore;
  countryTelCodes: CountryTelCode[] = countryTelCodes;

  selectedLanguage: LangInfo = {} as LangInfo;

  defaultTranslation = translations.find(
    (x) => x.code == this.selectedLanguage.code
  );

  translation: Translation = {} as Translation;

  selectedLangIndex: number = 0;
  selectedTranslation = this.defaultTranslation;
  filteredLangList = [] as LangInfo[];
  enableLangList: boolean = false;

  selectLanguage(language: LangInfo, index: number) {
    this.selectedLanguage = language;
    this.selectedLangIndex = this.restaurant.languages.findIndex(
      (lang) => lang === language
    );

    let code = this.selectedLanguage.code;
    this.selectedTranslation = translations.find((x) => x.code == code);
    if (this.selectedTranslation) {
      this.translation = this.selectedTranslation;
    }

    this.filteredLangList = this.restaurant.languages.filter(
      (lang) => lang !== language
    );
    this.enableLangList = false;
  }

  displayLangList(event: Event) {
    this.enableLangList = !this.enableLangList;
  }

  customer: Customer = {
    id: 0,
    surname: '',
    name: '',
    email: '',
    countryTelCode: { country: 'Greece', code: '+30' },
    phone: '',
    guests: '',
    optionStatus: '',
    checkTerms: false,
    arrivalTime: '',
  };

  // ========== constructor() ==========
  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private dataService: DataService,
    private renderer: Renderer2,
    @Inject(DOCUMENT) private document: Document
  ) {}
  // ========== ngOnInit() ==========
  ngOnInit(): void {
    console.log('ngOnInit() >>>>>>>>');

    // Βρες το id από το url
    if (this.route.snapshot.paramMap.get('id') !== null) {
      this.id = this.route.snapshot.paramMap.get('id') as string;
    }

    this.http
      .get(location.origin + `/assets/data/restaurants/${this.id}.json`)
      .subscribe((data) => {
        this.restaurant = data as CurrentStore;
        console.log('cssUrl: ' + this.restaurant.cssUrl);
        //========= Custom Styles ==========
        this.customStyles(
          this.restaurant.cssUrl
          // '../../assets/css/stylesCustom.css'
          // 'https://raw.githack.com/gCorrect/images-rep/main/i-host/stylesCustom.css'
        );

        // language
        this.selectedLanguage = this.restaurant.languages[0];
        this.filteredLangList = this.restaurant.languages.filter(
          (lang) => lang !== this.selectedLanguage
        );

        let code = this.selectedLanguage.code;
        let selectedTranslation: Translation = {} as Translation;
        // find the default or selected translation
        translations.forEach((element) => {
          if (element.code === code) {
            console.log('element : ' + JSON.stringify(element));
            selectedTranslation = element;
            this.defaultTranslation = selectedTranslation;
            this.translation = this.defaultTranslation;
            console.log(
              'translation : ' + JSON.stringify(this.translation.back)
            );
          }
        });
      }); // endOf subscribe(data)
    console.log('cssUrl: ' + this.restaurant.cssUrl);
  } // endOf ngOnInit()

  // Options (Screen#1)
  walkedIn: boolean = false;
  // ========== walkIn() ==========
  walkIn() {
    this.walkedIn = true;
  }

  waitedList: boolean = false;
  // ========== waitingList() ==========
  waitingList() {
    this.waitedList = true;
  }

  // Form (Screen#2)
  selectedCountryCode = this.customer.countryTelCode;
  // ========== selectCountryCode() ==========
  selectCountryCode(code: CountryTelCode) {
    this.selectedCountryCode = code;
    this.customer.countryTelCode = code;
  }
  // ========== toggleConfirm() ==========
  toggleConfirm() {
    this.customer.checkTerms = !this.customer.checkTerms;
  }
  isEmptyField: boolean = true;
  // ========== emptyFieldCheck() ==========
  emptyFieldCheck(input: any) {
    console.log('this.emptyFieldCheck>>>>>>>>>>>>>>.');
    console.log('check terms value: ' + this.customer.checkTerms);
    const submitBtn = document.getElementById('submit-btn');

    // if any field is empty
    if (
      // input.trim() === '' ||
      this.customer?.surname.trim() === '' ||
      this.customer?.name.trim() === '' ||
      this.customer?.email.trim() === '' ||
      this.customer?.countryTelCode === null ||
      this.customer?.phone.trim() === '' ||
      this.customer?.guests.trim() === '' ||
      this.invalidMail
    ) {
      //then
      this.isEmptyField = true;
      submitBtn?.setAttribute('disabled', '');
      return;
    } else {
      submitBtn?.removeAttribute('disabled');
      if (submitBtn !== null) {
        submitBtn.style.cursor = 'pointer';
      }
      this.isEmptyField = false;

      return;
    }
  }
  // ========== invalidMail() ==========
  invalidMail: boolean = false;
  validateEmail(email: string) {
    var validRegex =
      // /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (email.match(validRegex)) {
      this.invalidMail = false;
      this.emptyFieldCheck('');

      return true;
    } else {
      this.invalidMail = true;
      this.isEmptyField = true;
      if (email.trim() === '') {
        this.isEmptyField = true;
      }
      return false;
    }
  }
  // ========== invalidGuests() ==========
  invalidGuests: boolean = false;
  validateGuests(guests: string) {
    // var validRegex = /^\d{1,2,3}+$/;
    var validRegex = /^\d{1,2}$/;
    console.log('validateGuests ....');
    if (guests.match(validRegex)) {
      this.invalidPhone = false;
      this.emptyFieldCheck('');

      return true;
    } else {
      this.invalidGuests = true;
      this.isEmptyField = true;

      if (guests.trim() === '') {
        this.isEmptyField = true;
      }

      return false;
    }
  }

  // ========== invalidPhone() ==========
  invalidPhone: boolean = false;
  validatePhone(phone: string) {
    var validRegex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

    if (phone.match(validRegex)) {
      this.invalidPhone = false;
      this.emptyFieldCheck('');

      return true;
    } else {
      this.invalidPhone = true;
      this.isEmptyField = true;

      if (phone.trim() === '') {
        this.isEmptyField = true;
      }

      return false;
    }
  }
  submittedWalkIn: boolean = false;
  submittedWaitingList: boolean = false;
  // ========== submit() ==========
  submit() {
    console.log('submit()>>>>>>>>>>>>>>>>>>>>>>>');

    let date = new Date();
    let hours = date.getHours();
    let minutes = date.getMinutes();
    let seconds = date.getSeconds();
    let currentTime = `${hours}:${minutes}:${seconds}`;
    this.customer.arrivalTime = currentTime;
    console.log('customer data: ' + JSON.stringify(this.customer));

    if (this.walkedIn) {
      this.submittedWalkIn = true;
      // this.customer.enableWalkIn = true;
      this.customer.optionStatus = 'WalkIn/Περαστικός';

      this.dataService.addCustWalkIn(this.customer).subscribe(
        (res) => {
          console.log('addCustomer: ' + JSON.stringify(res));
        },
        (err) => {
          console.log('err : ' + JSON.stringify(err));
        }
      );
    } else if (this.waitedList) {
      this.submittedWaitingList = true;
      // this.customer.enableWaitingList = true;
      this.customer.optionStatus = 'Pending/Εκκρεμεί';

      this.dataService.addCustWaitList(this.customer).subscribe((res) => {
        console.log('addCustomer: ' + JSON.stringify(res));
      });
    }
  }

  // Redirect Button (Back) (Screens#2-#3)
  redirect() {
    this.walkedIn = false;
    this.waitedList = false;
    this.submittedWalkIn = false;
    this.submittedWaitingList = false;
  }
  // ========== customStyles() ==========
  customStyles(url: string) {
    console.log('customStyles() >>>>>>>>');
    const linkEl: HTMLLinkElement = this.renderer.createElement('link');
    linkEl.rel = 'stylesheet';
    linkEl.href = url;
    this.renderer.appendChild(this.document.head, linkEl);
  }
} // endOf OptionsPageComponent
